package com.atlassian.plugins.roadmap.models;

import java.util.Date;

public class Timeline {
    private Date startDate;
    private Date endDate;
    private DisplayOption displayOption;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public DisplayOption getDisplayOption() {
        return displayOption;
    }

    public void setDisplayOption(DisplayOption displayOption) {
        this.displayOption = displayOption;
    }

    public enum DisplayOption {
        MONTH,
        WEEK
    }
}
