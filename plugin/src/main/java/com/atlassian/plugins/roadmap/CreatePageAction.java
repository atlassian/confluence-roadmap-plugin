package com.atlassian.plugins.roadmap;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.xwork.FlashScope;
import com.atlassian.core.filters.ServletContextThreadLocal;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import static com.atlassian.confluence.plugins.createcontent.BlueprintConstants.CREATE_DIALOG_INIT_PARAMS_KEY;
import static com.atlassian.plugins.roadmap.NumberUtil.parseLongString;

public class CreatePageAction extends ConfluenceActionSupport {
    private static final String PAGE_VIEW_WITH_ID = "/pages/viewpage.action?pageId=%s&createDialog=true";

    private ContentEntityManager contentEntityManager;

    @Override
    public String execute() throws Exception {
        FlashScope.put(CREATE_DIALOG_INIT_PARAMS_KEY, Maps.newHashMap(
                ServletContextThreadLocal.getRequest().getParameterMap()));
        return super.execute();
    }

    public String getParentPage() {
        String contentId = ServletContextThreadLocal.getRequest().getParameter("roadmapContentId");
        String pageId = null;
        if (StringUtils.isBlank(contentId)) {
            pageId = ServletContextThreadLocal.getRequest().getParameter("parentPageId");
        } else {
            ContentEntityObject content = contentEntityManager.getById(parseLongString(contentId));
            if (content instanceof Comment) {
                pageId = ((Comment) content).getContainer().getIdAsString();
            } else if (content instanceof AbstractPage) {
                pageId = contentId;
            }
        }

        if (StringUtils.isNotBlank(pageId)) {
            return String.format(PAGE_VIEW_WITH_ID, pageId);
        } else {
            return "/dashboard.action?createDialog=true";
        }
    }

    public void setContentEntityManager(@Qualifier("contentEntityManager") ContentEntityManager contentEntityManager) {
        this.contentEntityManager = contentEntityManager;
    }
}
