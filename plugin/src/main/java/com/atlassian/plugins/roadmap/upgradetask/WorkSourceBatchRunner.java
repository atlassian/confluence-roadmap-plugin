package com.atlassian.plugins.roadmap.upgradetask;

import com.atlassian.confluence.content.render.xhtml.migration.BatchException;
import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static com.atlassian.util.concurrent.ThreadFactories.Type.DAEMON;
import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * A clone of {@link com.atlassian.confluence.content.render.xhtml.migration.WorkSourceBatchRunner}, with the Spring
 * {@link org.springframework.transaction.PlatformTransactionManager} replaced with a SAL {@link TransactionTemplate}.
 * <p>
 * This class is typically the basis for upgrade or migration tasks that need to manipulate a large amount of content.
 * It will use a parameterised number of worker threads to operate on batches of content as specified by a supplied
 * {@link BatchableWorkSource}.
 * </p>
 * <p>
 * Each batch of work will occur within its own transaction.
 * </p>
 *
 * @param <T> the type of item that will be in the batches
 */
public class WorkSourceBatchRunner<T> {
    private static final String THREAD_NAME = "roadmap-macro-migration";
    private static final int NUM_THREADS = 4;

    private final TransactionTemplate transactionTemplate;

    public WorkSourceBatchRunner(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * Exposed (as protected) to allow specification of the executor in unit tests.
     */
    protected List<Exception> run(final BatchableWorkSource<T> workSource, final BatchTask<T> task, ExecutorService executor) throws ExecutionException, InterruptedException {
        List<Future<List<Exception>>> futures = new ArrayList<>(workSource.numberOfBatches());
        for (int i = 0; i < workSource.numberOfBatches(); i++) {
            futures.add(executor.submit(() -> {
                final List<Exception> exceptions = new ArrayList<>(1);

                // wrap a transaction around each batch
                transactionTemplate.execute(() -> {
                    List<T> batch = workSource.getBatch();
                    for (int i1 = 0, size = batch.size(); i1 < size; i1++) {
                        try {
                            task.apply(batch.get(i1), i1, batch.size());
                        } catch (BatchException ex) {
                            exceptions.addAll(ex.getBatchExceptions());
                        } catch (
                                Exception ex) { // do not let exceptions propagate out and cause a rollback of the batch
                            exceptions.add(ex);
                        }
                    }
                    return null;
                });

                return exceptions;
            }));
        }

        List<Exception> allExceptions = new ArrayList<>(1);
        for (Future<List<Exception>> future : futures) {
            allExceptions.addAll(future.get()); // wait for all batches to complete
        }

        return allExceptions;
    }

    /**
     * Begin executing against all the work in the supplied work source. One or more threads will be spawned to perform
     * the work but this method will wait until all threads have finished before it returns.
     *
     * @param task the task to be run on each item in the batches from the work source
     * @return a list of any individual exceptions that occurred as the WorkSourceTask was executing. An empty list will
     * be returned if there are no exceptions.
     */
    public List<Exception> run(final BatchableWorkSource<T> workSource, final BatchTask<T> task) throws ExecutionException, InterruptedException {
        ExecutorService executor = newFixedThreadPool(NUM_THREADS, namedThreadFactory(THREAD_NAME, DAEMON));

        List<Exception> exceptions;
        try {
            exceptions = run(workSource, task, executor);
        } finally {
            executor.shutdown();
        }

        return exceptions;
    }
}
