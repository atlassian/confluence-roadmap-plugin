package com.atlassian.plugins.roadmap.beans;

import java.util.List;

public class Roadmap {
    public String title;
    public List<RoadmapColumn> columns;
    public List<RoadmapMarker> markers;
    public List<RoadmapTheme> themes;
}
