package com.atlassian.plugins.roadmap.renderer.helper;

import com.atlassian.plugins.roadmap.models.Timeline;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePosition;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePositionTitle;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Weeks;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Calculate relative position for row, lane, title,...
 */
public class TimeLineHelper {
    private static final String MONTH_KEY_PREFIX = "roadmap.editor.timeline.month";
    private static final long MILLISECONDS_A_DAY = 86400000; // 1000 * 60 * 60 * 24
    private static final long MILLISECONDS_A_WEEK = MILLISECONDS_A_DAY * 7;

    /**
     * Calculate all column position on timeline, all the offset for each column will be 1 (full column)
     *
     * @param timeline timeline
     * @return List of TimelinePosition
     */
    public static List<TimelinePosition> getColumnPosition(Timeline timeline) {
        int month = getMonth(timeline.getStartDate());
        List<TimelinePosition> roadmapColumns = new ArrayList<TimelinePosition>();
        int numberOfColumn;
        if (timeline.getDisplayOption() == Timeline.DisplayOption.MONTH) {
            numberOfColumn = getNumberOfColumnInMonthTimeline(timeline);
        } else {
            numberOfColumn = getNumberOfColumnInWeekTimeline(timeline);
        }
        for (int i = 0; i < numberOfColumn; i++) {
            TimelinePosition timelinePosition = new TimelinePosition(i, 1, month);
            roadmapColumns.add(timelinePosition);
        }
        return roadmapColumns;
    }

    /**
     * Calculate position of date in timeline
     *
     * @param timeline: RoadMap timeline
     * @param date:     date in timeline
     * @return relative position in timeline
     */
    public static TimelinePosition calculateTimelinePosition(Timeline timeline, Date date) {
        if (timeline.getDisplayOption() == Timeline.DisplayOption.MONTH) {
            return calculateMonthTimelinePosition(timeline, date);
        }
        return calculateWeekTimelinePosition(timeline, date);
    }

    private static TimelinePosition calculateMonthTimelinePosition(Timeline timeline, Date date) {
        int startMonthNumber = getMonth(timeline.getStartDate());
        Date firstDateOfMonth = DateUtils.truncate(date, Calendar.MONTH);
        int numberOfMonths = monthDiff(timeline.getStartDate(), date);

        long numberMilisecondFromStartMonth = date.getTime() - firstDateOfMonth.getTime();
        long numberMilisecondInMonth = getMillisecondInMonth(date);
        Double offset = (double) numberMilisecondFromStartMonth / numberMilisecondInMonth;

        return new TimelinePosition(numberOfMonths, offset, startMonthNumber);
    }

    private static TimelinePosition calculateWeekTimelinePosition(Timeline timeline, Date date) {
        Date timelineStartWeek = getStartDateOfWeek(timeline.getStartDate());
        Date startWeek = getStartDateOfWeek(date);
        int numberOfWeeks = weekDiff(timelineStartWeek, startWeek);
        long numberMilisecondFromStartWeek = date.getTime() - startWeek.getTime();
        Double offset = (double) numberMilisecondFromStartWeek / MILLISECONDS_A_WEEK;

        return new TimelinePosition(numberOfWeeks, offset, numberOfWeeks);
    }

    /**
     * This is temporary support to get position title by month
     *
     * @param timeline         timeline
     * @param timelinePosition {TimelinePosition}
     * @param i18n             i18n
     * @return {String} as title for timelinePosition
     */
    public static TimelinePositionTitle getPositionTitle(Timeline timeline, TimelinePosition timelinePosition, I18nResolver i18n) {
        if (timeline.getDisplayOption() == Timeline.DisplayOption.MONTH) {
            return getMonthPositionTitle(timeline, timelinePosition, i18n);
        }
        return getWeekPositionTitle(timeline, timelinePosition, i18n);
    }

    private static TimelinePositionTitle getMonthPositionTitle(Timeline timeline, TimelinePosition timelinePosition, I18nResolver i18n) {
        int month = (timelinePosition.getColumn() + timelinePosition.getColumnOffset()) % 12;
        String monthString = i18n.getText(MONTH_KEY_PREFIX + String.valueOf(month + 1));
        Calendar startTime = getCalendar(timeline.getStartDate());
        int year = startTime.get(Calendar.YEAR) + (timelinePosition.getColumn() + timelinePosition.getColumnOffset()) / 12;
        return new TimelinePositionTitle(monthString, String.valueOf(year));
    }

    private static TimelinePositionTitle getWeekPositionTitle(Timeline timeline, TimelinePosition timelinePosition, I18nResolver i18n) {
        Date startDate = getStartDateOfWeek(timeline.getStartDate());
        Calendar calendar = getCalendar(startDate);

        calendar.add(Calendar.DATE, timelinePosition.getColumn() * 7);
        String timelineTitle = getWeekTimelineTitle(calendar, i18n);
        int year = calendar.get(Calendar.YEAR);
        return new TimelinePositionTitle(timelineTitle.toString(), String.valueOf(year));
    }

    public static int getNumberOfColumnInTimeline(Timeline timeline) {
        if (timeline.getDisplayOption() == Timeline.DisplayOption.MONTH)
            return getNumberOfColumnInMonthTimeline(timeline);
        return getNumberOfColumnInWeekTimeline(timeline);
    }

    private static int getNumberOfColumnInMonthTimeline(Timeline timeline) {
        return Math.max(monthDiff(timeline.getStartDate(), timeline.getEndDate()) + 1, 1);
    }

    private static int getNumberOfColumnInWeekTimeline(Timeline timeline) {
        Date startDate = getStartDateOfWeek(timeline.getStartDate());
        Date endDate = getEndDateOfWeek(timeline.getEndDate());
        return weekDiff(startDate, endDate) + 1;
    }

    private static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private static int getMonth(Date date) {
        Calendar startTime = getCalendar(date);
        return startTime.get(Calendar.MONTH);
    }

    private static long getMillisecondInMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int numberOfDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return (long) numberOfDay * 86400000;// 24 * 60 * 60 * 1000;
    }

    private static int monthDiff(Date startDate, Date endDate) {
        Calendar startTime = getCalendar(startDate);
        Calendar endTime = getCalendar(endDate);

        int diffYear = endTime.get(Calendar.YEAR) - startTime.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + endTime.get(Calendar.MONTH) - startTime.get(Calendar.MONTH);
        return diffMonth;
    }

    private static int weekDiff(Date date1, Date date2) {
        DateTime dateTime1 = new DateTime(date1);
        DateTime dateTime2 = new DateTime(date2);
        return Weeks.weeksBetween(dateTime1, dateTime2).getWeeks();
    }

    private static Date getStartDateOfWeek(Date date) {
        Calendar calendar = getCalendar(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        day = ((day == Calendar.SUNDAY) ? -6 : Calendar.MONDAY - day);
        calendar.add(Calendar.DATE, day);
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));
        return calendar.getTime();
    }

    private static Date getEndDateOfWeek(Date date) {
        Calendar calendar = getCalendar(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        day = ((day == Calendar.SUNDAY) ? 0 : 8 - day);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    private static String getWeekTimelineTitle(Calendar calendar, I18nResolver i18n) {
        StringBuilder weekTimelineTitle = new StringBuilder();
        weekTimelineTitle.append(String.format("%02d", calendar.get(Calendar.DATE)));
        weekTimelineTitle.append("-");
        weekTimelineTitle.append(i18n.getText(MONTH_KEY_PREFIX + String.valueOf(calendar.get(Calendar.MONTH) + 1)));
        return weekTimelineTitle.toString();
    }
}


