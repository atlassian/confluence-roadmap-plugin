package com.atlassian.plugins.roadmap.upgradetask;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.confluence.search.v2.DefaultSearchResults;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.util.concurrent.atomic.AtomicInteger;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Provides a worksource from a searchQuery, the query should return searchResults that can be converted
 * ContentEntityObjects
 */
class SearchResultsBatchWorkSource<T> implements BatchableWorkSource<T> {
    private static final Logger log = LoggerFactory.getLogger(SearchResultsBatchWorkSource.class);

    private final SearchManager searchManager;
    private final ImmutableList<SearchResult> searchResults;
    private final int batchSize;
    private final Function<Searchable, T> transformer;

    private final AtomicInteger offset = new AtomicInteger();

    /**
     * @param searchManager
     * @param searchResults
     * @param batchSize
     * @param transformer   - the function used to transform the list of Searchables returned from the query to the type required
     *                      by the batch, may also perform filtering, so it is safe for the apply method to return null, the apply
     */
    public SearchResultsBatchWorkSource(SearchManager searchManager, List<SearchResult> searchResults, int batchSize,
                                        Function<Searchable, T> transformer) {
        this.searchManager = searchManager;
        this.searchResults = ImmutableList.copyOf(searchResults);
        this.batchSize = batchSize;
        this.transformer = transformer;
    }

    /**
     * gets the list of entities that form the batch. The list is retrieved by executing the query and converting the
     * search result to a list of entities, it is only then that the transformer is returned, so the number of entities
     * per batch as returned from this method may differ from the batch size, and may in fact be empty. An empty list
     * returned from this method does not connote that there are no more batches, rather @see hasMoreBatches
     *
     * @return the entities in this batch, the list does not contain any null values.
     */
    @Override
    public List<T> getBatch() {
        // TODO review thread safety
        int oldOffset = 0;
        int newOffset = 0;
        do {
            oldOffset = offset.get();
        } while (!offset.compareAndSet(oldOffset, newOffset = (oldOffset + batchSize)));

        if (oldOffset >= searchResults.size()) {
            return Collections.emptyList();
        }


        int endIndex = Math.min(newOffset, searchResults.size());
        // convert search results to list of searchables
        final List<Searchable> searchables = searchManager.convertToEntities(new DefaultSearchResults(
                searchResults.subList(oldOffset, endIndex), batchSize), SearchManager.EntityVersionPolicy.INDEXED_VERSION);

        // now apply the transform into the desired type
        return new LinkedList<>(
                Collections2.filter(
                        searchables.stream().map(transformer).collect(Collectors.toList()),
                        Objects::nonNull));
    }

    @Override
    public boolean hasMoreBatches() {
        return offset.get() < searchResults.size();
    }

    @Override
    public int numberOfBatches() {
        return (searchResults.size() / batchSize) + 1;
    }

    @Override
    public void reset(int total) {
        offset.set(0);
    }

    @Override
    public int getTotalSize() {
        return searchResults.size();
    }
}
