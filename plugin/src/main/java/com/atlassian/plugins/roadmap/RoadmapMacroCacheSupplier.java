package com.atlassian.plugins.roadmap;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.plugins.roadmap.TimelinePlannerMacroManager.LinkStatus;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;
import com.atlassian.util.concurrent.Lazy;
import com.atlassian.util.concurrent.Supplier;

import static java.util.concurrent.TimeUnit.HOURS;

class RoadmapMacroCacheSupplier {

    private final Supplier<Cache<String, byte[]>> imageCacheRef;
    private final Supplier<Cache<String, String>> macroSourceCacheRef;
    private final Supplier<Cache<String, LinkStatus>> linkStatusCacheRef;
    private final Supplier<Cache<String, RoadmapPageLink>> pageLinkCacheRef;

    public static final String IMAGE_CACHE_NAME = "RoadmapMacroImages";
    public static final String MACRO_SOURCE_CACHE_NAME = "RoadmapMacroSources";
    public static final String LINK_STATUS_CACHE_NAME = "RoadmapMacroLinkStatuses";
    public static final String PAGE_LINK_CACHE_NAME = "RoadmapMacroPageLinks";

    RoadmapMacroCacheSupplier(CacheManager cacheFactory) {
        this.imageCacheRef = Lazy.supplier(() -> createImageCache(cacheFactory));
        this.macroSourceCacheRef = Lazy.supplier(() -> createMacroSourceCache(cacheFactory));
        this.linkStatusCacheRef = Lazy.supplier(() -> createLinkStatusCache(cacheFactory));
        this.pageLinkCacheRef = Lazy.supplier(() -> createPageLinkCache(cacheFactory));
    }

    private static Cache<String, byte[]> createImageCache(CacheFactory cacheFactory) {
        return cacheFactory.getCache(
                IMAGE_CACHE_NAME,
                null,
                new CacheSettingsBuilder().remote().build()
        );
    }

    private static Cache<String, String> createMacroSourceCache(CacheFactory cacheFactory) {
        return cacheFactory.getCache(
                MACRO_SOURCE_CACHE_NAME,
                null,
                new CacheSettingsBuilder().remote().build()
        );
    }

    private static Cache<String, LinkStatus> createLinkStatusCache(CacheFactory cacheFactory) {
        return cacheFactory.getCache(
                LINK_STATUS_CACHE_NAME,
                null,
                new CacheSettingsBuilder().remote().expireAfterWrite(2, HOURS).build()
        );
    }

    private static Cache<String, RoadmapPageLink> createPageLinkCache(CacheFactory cacheFactory) {
        return cacheFactory.getCache(
                PAGE_LINK_CACHE_NAME,
                null,
                new CacheSettingsBuilder().remote().expireAfterWrite(2, HOURS).build()
        );
    }

    public Cache<String, byte[]> getImageCache() {
        return imageCacheRef.get();
    }

    public Cache<String, String> getMarcoSourceCache() {
        return macroSourceCacheRef.get();
    }

    public Cache<String, LinkStatus> getLinkStatusCache() {
        return linkStatusCacheRef.get();
    }

    public Cache<String, RoadmapPageLink> getPageLinkCache() {
        return pageLinkCacheRef.get();
    }
}
