package com.atlassian.plugins.roadmap.renderer.beans;

/**
 * Bean to keep position information in {com.atlassian.plugins.roadmap.models.Timeline}
 */
public class TimelinePosition {
    private int column; //base on the timeline option, it should be split to 4, 12
    private double offset; //a related number base on column. Ex: 0.5 for a center of column, 1: full column
    private int columnOffset; //column shift up (recoordinate the column, ex: we want to consider column 4 as column 0)

    public TimelinePosition() {
    }

    public TimelinePosition(int column, double offset, int columnOffset) {
        this.column = column;
        this.columnOffset = columnOffset;
        this.offset = offset;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public int getColumnOffset() {
        return columnOffset;
    }

    public void setColumnOffset(int columnOffset) {
        this.columnOffset = columnOffset;
    }

    @Override
    public String toString() {
        return String.format("[Column=%d, Offset=%f, ColumnOffset=%d]", column, offset, columnOffset);
    }
}
