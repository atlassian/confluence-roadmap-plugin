package com.atlassian.plugins.roadmap.beans;

import java.util.List;

public class RoadmapTheme {
    public String title, colour;
    public List<RoadmapTask> tasks;
}
