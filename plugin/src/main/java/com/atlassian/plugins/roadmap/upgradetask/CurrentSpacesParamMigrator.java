package com.atlassian.plugins.roadmap.upgradetask;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.CustomContentManager;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.migration.AbstractExceptionTolerantMigrator;
import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.confluence.content.render.xhtml.migration.exceptions.MigrationException;
import com.atlassian.confluence.core.BodyContent;
import com.atlassian.confluence.core.BodyType;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.query.MacroUsageQuery;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionUpdater;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Searches for all usages of the Roadmap Planner macro and remove the "currentspaces" parameter.
 *
 * @see CurrentSpacesParamUpgradeTask
 */

//@Component("migrator")
class CurrentSpacesParamMigrator {
    private static final Logger log = LoggerFactory.getLogger(CurrentSpacesParamMigrator.class);

    private static final String CURRENT_SPACES_PARAM = "currentspaces";
    private static final int BATCH_SIZE = 50;

    private final SearchManager searchManager;
    private final CustomContentManager contentManager;
    private final WorkSourceBatchRunner<ContentEntityObject> batchRunner;
    private final XhtmlContent xhtmlContent;

    private volatile boolean inProgress = false;
    private AtomicInteger numFailed;
    private AtomicInteger numMigrated;
    private AtomicInteger numMigrationNotRequired;

    private final Function<Searchable, ContentEntityObject> searchableToCEOTransformer =
            from -> {
                if (from instanceof ContentEntityObject) {
                    BodyContent bodyContent = ((ContentEntityObject) from).getBodyContent();
                    if (bodyContent.getBodyType().equals(BodyType.XHTML)) {
                        return (ContentEntityObject) from;
                    }
                }
                return null;
            };

    //    @Autowired
    public CurrentSpacesParamMigrator(XhtmlContent xhtmlContent, SearchManager searchManager,
                                      CustomContentManager contentManager, WorkSourceBatchRunner<ContentEntityObject> batchRunner) {
        this.contentManager = contentManager;
        this.searchManager = searchManager;
        this.batchRunner = batchRunner;
        this.xhtmlContent = xhtmlContent;
    }

    public void migrate() throws MigrationException {
        synchronized (this) {
            if (inProgress) {
                throw new IllegalStateException("Another 'currentspaces' parameter migration is currently in progress");
            }
            resetMigrationFlags();
        }

        try {
            doMigration();
        } catch (InvalidSearchException e) {
            throw new MigrationException("Error searching for macro usages", e);
        } catch (Exception e) {
            throw new MigrationException(e);
        } finally {
            inProgress = false;
        }
    }

    private void resetMigrationFlags() {
        inProgress = true;
        numFailed = new AtomicInteger(0);
        numMigrated = new AtomicInteger(0);
        numMigrationNotRequired = new AtomicInteger(0);
    }

    private void doMigration() throws Exception {
        log.info("Started migration of Roadmap macros");

        migrateCurrentContents();

        log.info("Finished migration of Roadmap macros:\n"
                + numMigrated + " Roadmap macro(s) were updated\n"
                + numMigrationNotRequired + " Roadmap macro(s) did not require any changes\n"
                + numFailed + " Roadmap macro(s) could not be updated\n");
    }

    private SearchResults findPagesWithRoadmapMacro() throws InvalidSearchException {
        SearchQuery query = new MacroUsageQuery("roadmap");
        ISearch search = new ContentSearch(query, null, 0, Integer.MAX_VALUE);

        return searchManager.search(search);
    }

    private void migrateCurrentContents() throws Exception {
        final SearchResults searchResults = findPagesWithRoadmapMacro();
        final BatchableWorkSource<ContentEntityObject> workSource =
                new SearchResultsBatchWorkSource<ContentEntityObject>(
                        searchManager, searchResults.getAll(), BATCH_SIZE, searchableToCEOTransformer::apply);
        final BatchTask<ContentEntityObject> batchTask = new ContentEntityMigrationBatchTask(
                new RoadmapMacroParamsContentEntityMigrator(), contentManager);

        batchRunner.run(workSource, batchTask);
    }

    class RoadmapMacroParamsContentEntityMigrator extends AbstractExceptionTolerantMigrator {
        @Override
        public MigrationResult migrate(String content, final ConversionContext conversionContext) {
            try {
                final List<Boolean> migrations = newArrayList();

                final String migratedContent = xhtmlContent.updateMacroDefinitions(content,
                        conversionContext, new MacroDefinitionUpdater() {
                            @Override
                            public MacroDefinition update(MacroDefinition macroDefinition) {
                                if (StringUtils.equals(macroDefinition.getName(), "roadmap")) {
                                    final Map<String, String> params = macroDefinition.getParameters();
                                    if (params.containsKey(CURRENT_SPACES_PARAM)) {
                                        migrations.add(true);
                                        processParams(macroDefinition);
                                        numMigrated.incrementAndGet();
                                    } else {
                                        numMigrationNotRequired.incrementAndGet();
                                    }
                                }

                                return macroDefinition;
                            }
                        });
                return new MigrationResult(migratedContent, (migrations.size() > 0));
            } catch (XhtmlException e) {
                log.info("Encountered an exception during Roadmap macro migration", e);
                numFailed.incrementAndGet();
                return new MigrationResult(content, false);
            }
        }

        private void processParams(MacroDefinition macroDefinition) {
            macroDefinition.setParameter(CURRENT_SPACES_PARAM, null);
            macroDefinition.setTypedParameter(CURRENT_SPACES_PARAM, null);
        }
    }
}
