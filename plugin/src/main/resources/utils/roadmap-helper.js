(function($, _) {
    var RM = Confluence.Roadmap = Confluence.Roadmap || {};
    RM.Helper = {
        getPosXOnMonthTimeline: function(timeline, date) {
            const startDate = Confluence.Roadmap.DateUtilities.parseDate(timeline.startDate);
            const dateToBeginRender = Confluence.Roadmap.DateUtilities.parseDate(date);
            var months = RM.DateUtilities.getNumberOfMonths(
                RM.DateUtilities.getStartOfMonth(startDate),
                RM.DateUtilities.getStartOfMonth(dateToBeginRender)
            );
            var posX = months * Roadmap.MONTH_WIDTH; // posX base on month period
            var pixelFromStartMonth = RM.DateUtilities.getMillisecondsFromStartMonth(dateToBeginRender) * Roadmap.MONTH_WIDTH / RM.DateUtilities.getMillisecondsInMonth(dateToBeginRender);
            return Math.round(posX + pixelFromStartMonth);
        },

        /**
         * Get CSS left position for a bar by start date of timeline and start date of the bar.
         *
         * @param {Object} timeline
         * @param {Date} timeline.startDate - start date of timeline
         * @param {Date} timeline.endDate - end date of timeline
         * @param {string} timeline.displayOption - WEEK or MONTH
         *
         * @param {Array} timeline.displayTimelineColumns
         * @param {Date} startDate - start date object of a bar
         * @returns {number} CSS left position, is negative number if timeline start day is after given bar start day.
         */
        getPosXOnWeekTimeline: function(timeline, startDate) {
            const date = Confluence.Roadmap.DateUtilities.parseDate(timeline.startDate);
            const startDateTimelineStartFromMonday = RM.DateUtilities.getStartOfIsoWeek(date);
            const weeks = (startDate - startDateTimelineStartFromMonday) / RM.DateUtilities.MILLISECONDS_A_WEEK;
            const posX = weeks * Roadmap.WEEK_WIDTH;
            return Math.round(posX);
        },

        getWidthTimeline: function(timeline, bar) {
            var barWidth = bar.duration;
            if (timeline.displayOption === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                barWidth *= Roadmap.MONTH_WIDTH;
            } else {
                barWidth *= Roadmap.WEEK_WIDTH;
            }

            return barWidth < Roadmap.EXTRA_SPACE_AROUND
                    ? 0
                    : Math.round(barWidth - Roadmap.EXTRA_SPACE_AROUND);
        },

        getMonthStartDateByPosition: function(timeline, positionX) {
            const months = Math.floor(positionX / Roadmap.MONTH_WIDTH);
            const startDate = RM.DateUtilities.parseDate(timeline.startDate);
            const timelineStartMonth = RM.DateUtilities.getStartOfMonth(startDate);
            timelineStartMonth.setMonth(timelineStartMonth.getMonth() + months);
            const daysWidth = Math.abs(positionX) % Roadmap.MONTH_WIDTH;
            if (daysWidth !== 0 && months < 0) {
                daysWidth = Roadmap.MONTH_WIDTH - daysWidth;
            }

            const msFromMonth = RM.DateUtilities.getMillisecondsInMonth(timelineStartMonth) * daysWidth / Roadmap.MONTH_WIDTH;
            return new Date(timelineStartMonth.getTime() + msFromMonth);
        },

        getWeekStartDateByPosition: function(timeline, positionX) {
            const startDate = RM.DateUtilities.parseDate(timeline.startDate);
            const startDateTimeline = Confluence.Roadmap.DateUtilities.getStartOfIsoWeek(startDate);
            const numberOfWeeks = positionX / Roadmap.WEEK_WIDTH;

            return new Date(startDateTimeline.setDate(startDateTimeline.getDate() + numberOfWeeks * 7));
        },

        guid: function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    };

    RM.ColorHelper = {
        resetColorCounts: function() {
            _.each(Roadmap.COLORS, function(color) {
                color.count = 0;
            });
        },

        findColor: function(color) {
            return _.find(Roadmap.COLORS, function(roadmapColor) {
                return roadmapColor.lane === color.lane;
            });
        },

        adjustColorCount: function(color, adjustAmount) {
            var roadmapColor = RM.ColorHelper.findColor(color);
            if (roadmapColor) {
                roadmapColor.count = roadmapColor.count + adjustAmount;
            }
        },

        getColor: function() {
            return _.min(Roadmap.COLORS, function(color) {
                return color.count;
            });
        }
    };
}(AJS.$, window._));
