(function($, _) {
    var RM = Confluence.Roadmap = Confluence.Roadmap || {};
    var USER_LOCALE = AJS.Meta.get('user-locale').replace('_', '-');

    RM.DateUtilities = {
        MILLISECONDS_A_DAY: 1000 * 60 * 60 * 24,
        MILLISECONDS_A_WEEK: 1000 * 60 * 60 * 24 * 7,

        getStartOfIsoWeek: date => new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1),
        getEndOfIsoWeek: date => new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7),
        getStartOfMonth: date => new Date(date.getFullYear(), date.getMonth(), 1),
        getEndOfMonth: date => new Date(new Date(date.getFullYear(), date.getMonth() + 1, 1).getTime() - 1), // moment.js implementation
        addMonths: (d, n) => {
            const date = new Date(d);
            const day = date.getDate();
            date.setDate(1);
            date.setMonth(date.getMonth() + n);
            const year = date.getFullYear();
            const isLeapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
            const daysInMonth = [31, (isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][date.getMonth()]
            date.setDate(Math.min(day, daysInMonth));
            return date;
        },
        areInSameWeek: (date1, date2) => {
            const date1Rounded = new Date(new Date(date1).setHours(0, 0, 0, 0));
            const date2Rounded = new Date(new Date(date2).setHours(0, 0, 0, 0));
            return new Date(date1Rounded.setDate(date1Rounded.getDate() - date1Rounded.getDay())).getTime()
                === new Date(date2Rounded.setDate(date2Rounded.getDate() - date2Rounded.getDay())).getTime();
        },
        setIsoWeekday: (date, weekday) => new Date(date.setDate(date.getDate() - date.getDay() + weekday)),
        diffInYears: (fromDate, toDate) =>
            Math.abs(new Date(toDate.getTime() - fromDate.getTime()).getUTCFullYear() - 1970),
        /**
         * Try to parse a string to a date. If can not parse, return null.
         * @param {string|number|date} input input to be parsed
         * @return {Date | null} return parsed date or null
         */
        parseDate: function(input) {
            if (!input) {
                return null;
            }
            if (input instanceof Date || typeof input === 'number') {
                return new Date(input);
            }
            if (input.includes && input.includes(':')) {
                return new Date(input);
            }
            const parts = input.split('-');
            const year = parts[0].toString();
            const currentYear = new Date().getFullYear().toString();
            parts[0] = currentYear.substring(0, currentYear.length - year.length) + year;

            const timestamp = Date.parse(parts.join('-'));
            if (isNaN(timestamp)) {
                return null;
            } else {
                return new Date(new Date(parts[0], parts[1] - 1, parts[2]));
            }
        },

        /**
         * Get date string from date
         *
         * @param {Date} date value
         * @param {string} format format pattern string
        */
        getDateStringFromDate: function(date, format) {
            switch (format) {
                case Roadmap.SHORT_DATE_FORMAT:
                    return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, 0)}-${date.getDate().toString().padStart(2, 0)}`;
                case Roadmap.WEEK_FORMAT:
                    return date.getDate().toString().padStart(2, '0') + '-' + date.toLocaleString(USER_LOCALE, { month: "short" });
                case Roadmap.MONTH_FORMAT:
                    return date.toLocaleString(USER_LOCALE, { month: 'short' });
                default:
                    return date.getFullYear().toString() + '-'
                        + (date.getMonth() + 1).toString().padStart(2, '0') + '-'
                        + date.getDate().toString().padStart(2, '0') + ' '
                        + date.getHours().toString().padStart(2, '0') + ':'
                        + date.getMinutes().toString().padStart(2, '0') + ':'
                        + date.getSeconds().toString().padStart(2, '0');
            }
        },

        /**
         * Get number of months within a period
         *
         * @param {Date} startDate
         * @param {Date} endDate
         * @return {number} number of months within the period
         */
        getNumberOfMonths: function(startDate, endDate) {
            return endDate.getMonth() - startDate.getMonth() + (endDate.getFullYear() - startDate.getFullYear()) * 12;
        },

        getMillisecondsInMonth: function(date) {
            return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() * RM.DateUtilities.MILLISECONDS_A_DAY;
        },

        getMillisecondsFromStartMonth: function(date) {
            return date.getTime() - new Date(date.getFullYear(), date.getMonth(), 1).getTime();
        },

        /**
         * calculate duration information of a moment date
         *
         * @param {Date} date - date
         * @returns {Object} duration information object:
         *                      totalMsOfMonth: total Milliseconds of month get from date.
         *                      MsFromStartMonth: Milliseconds from the month's beginning date to date.
         *                      MsRemainingOfMonth: Milliseconds from date to the month's ending date.
         *                      durationOfMonth: month's duration from the month's beginning date to date.
         *                      remainingDurationOfMonth: month's duration from date to the month's ending date.
         */
        calculateDurationInformation: function(date) {
            var totalMsOfMonth = RM.DateUtilities.getMillisecondsInMonth(date);
            var msFromStartMonth = RM.DateUtilities.getMillisecondsFromStartMonth(date);
            var msRemainingOfMonth = totalMsOfMonth - msFromStartMonth;

            return {
                totalMsOfMonth: totalMsOfMonth,
                msFromStartMonth: msFromStartMonth,
                msRemainingOfMonth: msRemainingOfMonth,
                durationOfMonth: msFromStartMonth / totalMsOfMonth,
                remainingDurationOfMonth: msRemainingOfMonth / totalMsOfMonth
            };
        },

        /**
         * Get week duration by a start date and month duration.
         *
         * @param {Date} startDate - start date native JS object.
         * @param {number} monthDuration - can be a floated number.
         * @returns {number} week duration, can be floated number with 2 decimal digits.
         */
        convertToWeekDuration: function(startDate, monthDuration) {
            let endDate = new Date(startDate);
            const startDateDurationInformation = RM.DateUtilities.calculateDurationInformation(startDate);

            if (monthDuration <= startDateDurationInformation.remainingDurationOfMonth) {
                const durationInMs =
                    monthDuration * startDateDurationInformation.totalMsOfMonth;
                endDate = new Date(endDate.getTime() + durationInMs);
            } else {
                endDate = new Date(endDate.getTime() +
                    startDateDurationInformation.msRemainingOfMonth);
                monthDuration =
                    monthDuration - startDateDurationInformation.remainingDurationOfMonth;
                const monthCount = Math.floor(monthDuration);
                if (monthCount > 0) {
                    endDate.setMonth(endDate.getMonth() + monthCount);
                }

                const remainingDuration = monthDuration % 1;
                const totalMsOfEndMonth = RM.DateUtilities.getMillisecondsInMonth(endDate);
                const remainingMs = remainingDuration * totalMsOfEndMonth;
                endDate = new Date(endDate.getTime() + remainingMs);
            }

            result = Math.abs(endDate.getTime() - startDate.getTime()) / RM.DateUtilities.MILLISECONDS_A_WEEK;
            return result;
        },

        /**
         * Get month duration by a start date and week duration.
         *
         * @param {Date} startDate - start date native JS object
         * @param {number} weekDuration
         * @returns {number} month duration, can be floated number with 2 decimal digits.
         */
        convertToMonthDuration: function(startDate, weekDuration) {
            const durationInMs = weekDuration * RM.DateUtilities.MILLISECONDS_A_WEEK;
            const endDate = new Date(startDate.getTime() + durationInMs);

            let monthCount = endDate.getFullYear() * 12 + endDate.getMonth();
            monthCount =
                monthCount - (startDate.getFullYear() * 12 + startDate.getMonth());

            const startDateDurationInformation = RM.DateUtilities.calculateDurationInformation(
                startDate
            );

            let result;
            if (monthCount === 0) {
                result = durationInMs / startDateDurationInformation.totalMsOfMonth;
            } else {
                let monthDuration = startDateDurationInformation.remainingDurationOfMonth;

                const endDateDurationInformation = RM.DateUtilities.calculateDurationInformation(
                    endDate
                );
                monthDuration = monthDuration + endDateDurationInformation.durationOfMonth;

                monthDuration = monthDuration + (monthCount - 1);
                result = monthDuration;
            }
            return result;
        }
    };

    RM.FieldUtilities = {
        /**
         * Fix some issues for AUI date picker
         *
         * @param {$container} find date pickers in container
         */
        fixDatePickerFields: function($container) {
            // fix some issues relate to AUI datepicker by apply datepicker-patch.css
            // fix datepicker doesn't fire change when AUI versions which < 5.4.5
            var $datepickers = $container.find('input[data-aui-dp-uuid]');
            $datepickers.each(function(index, element) {
                var $dp = $(element),
                    uuid = $dp.attr('data-aui-dp-uuid');
                $dp.on('click focus', function() {
                    var $dpPopup = $('[data-aui-dp-popup-uuid=' + uuid + ']');
                    if (AJS.version <= '5.4.5') {
                        var defaultOnSelectHandler = $dpPopup.datepicker('option', 'onSelect');
                        $dpPopup.datepicker('option', 'onSelect', function(dateText, inst) {
                            defaultOnSelectHandler(dateText, inst);
                            $dp.change();
                        });
                    }
                    $dpPopup.parents('.aui-inline-dialog').addClass('datepicker-roadmap-patch');
                });
            });
        }
    };
}(AJS.$, window._));
