(function($, _) {
    var FormStateControl = require('confluence/form-state-control');

    var InlineDialogView = Roadmap.InlineDialogView;
    Roadmap.LaneRenameDialogView = InlineDialogView.extend({
        events: {
            'click .rename-button': '_onRenameButtonClick',
            'keyup .lane-title': '_onChangeLaneTitle'
        },

        initialize: function() {
            this.options.dialogOptions = {
                width: this._getDialogWidth()
            };

            InlineDialogView.prototype.initialize.call(this, this.options);
        },

        _getContent: function(element) {
            element.html(Confluence.Templates.Roadmap.laneRenameDialog({laneTitle: this.options.trigger.text()}));
            
            this.controls = {
                $renameButton: this.$el.find('.rename-button'),
                $laneTitle: this.$el.find('.lane-title')
            };
        },

        _getDialogWidth: function() {
            return 315;
        },

        _onChangeLaneTitle: function(e) {
            var changedTitle = $.trim(this.controls.$laneTitle.val());
            if (!changedTitle) {
                FormStateControl.disableElement(this.controls.$renameButton);
            } else {
                FormStateControl.enableElement(this.controls.$renameButton);
                if (e.which == AJS.$.ui.keyCode.ENTER) {
                    this.controls.$renameButton.click(); //bug in IE only (submit with enter)
                }
            }
        },

        _onShow: function(e) {
            this.controls.$laneTitle.select();
        },

        _onRenameButtonClick: function(event) {
            event.preventDefault();
            this.hide();
            var changedTitle = this.controls.$laneTitle.val();
            this.options.lane.renameTitle(changedTitle);
        }
    });
})(AJS.$, window._);