(function($, _) {
    window.Roadmap = window.Roadmap || {};
    Roadmap.TimelineColumnView = Backbone.View.extend({
        className: 'roadmap-frame-content',
        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
            var numberOfMonths = this.model.get('displayTimelineColumns').length;

            this.$el.html(Confluence.Templates.Roadmap.timelineColumn({numberOfColumns: numberOfMonths}));
            return this;
        },

        updateHeight: function(height) {
            this.$el.height(height);
        },

        update: function() {
            this.render();
        },

        getSizeAndPosition: function() {
            return {
                width: this.$el.width(),
                height: this.$el.height(),
                left: this.$el.position().left,
                top: this.$el.position().top
            };
        }
    });
})(AJS.$, window._);