(function($, _) {
    Roadmap.BarRowView = Backbone.View.extend({
        className: 'roadmap-bar-row',

        initialize: function() {
            _.bindAll(this, '_addEvents');
            this._addEvents();
            return this;
        },

        attributes: function() {
            return {
                cid: this.cid
            };
        },

        _addEvents: function() {
            var me = this;
            this.$el.droppable({
                greedy: true,
                tolerance: 'pointer',
                hoverClass: 'roadmap-bar-drag-hover',
                activeClass: 'roadmap-bar-drag-active',
                accept: '.roadmap-bar',
                over: function(e, ui) {
                    Roadmap.DragDrop.barDraggingOver[me.cid] = me;

                    if (ui.helper.data('isAddingNewRow') !== true) {
                        var $barRow = $(this);
                        $barRow.closest('.roadmap-content').find('.new-row-placeholder').remove();
                    } else {
                        me.$el.removeClass('roadmap-bar-drag-hover');
                    }
                },
                out: function() {
                    var $barRow = $(this);
                    delete Roadmap.DragDrop.barDraggingOver[me.cid];
                    $barRow.closest('.roadmap-content').find('.new-row-placeholder').remove();
                },
                drop: function(e, ui) {
                    var isBarOverlapped = _.reject(Roadmap.DragDrop.barDraggingOver, function(item) {
                        return (typeof item === 'object');
                    }).length > 0;

                    if (!ui.helper.hasClass('roadmap-bar-overlapped') && !isBarOverlapped) {
                        $(this).trigger('BarView.drop', [
                            {
                                $barViewHelper: ui.helper,
                                $barViewOriginal: ui.draggable,
                                $barRow: $(this)
                            },
                            ui
                        ]);
                    }
                }
            });
        },

        isEmptyRow: function() {
            return this.$el.is(':empty');
        }
    });
})(AJS.$, window._);