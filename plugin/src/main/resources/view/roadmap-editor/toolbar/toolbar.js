(function($, _) {
    window.Roadmap = window.Roadmap || {};
    Roadmap.ToolbarView = Backbone.View.extend({
        className: 'roadmap-toolbar',
        events: {
            'click #toolbar-button-add-bar': 'onAddBarClick',
            'click #toolbar-button-add-lane': 'onAddLaneClick',
            'click #toolbar-button-add-marker': 'onAddMarkerClick',
            'change #timeline-display-options-select': 'onTimelineOptionChange'
        },
        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
            // This demonstrate you how to use SOY template in View
            $(this.$el).html(Confluence.Templates.Roadmap.toolbar());
            this._initVariables();
            this._setupTimelineOptionsForm();
            return this;
        },

        /**
         * Add new Bar to the first Lane
         */
        onAddBarClick: function(e) {
            e.preventDefault();

            var firstLane = this.model.get('lanes').at(0); // by default, use first lane
            var bars = firstLane.get('bars');

            var timeline = this.model.get('timeline');
            var startDateTimelineActual = Confluence.Roadmap.DateUtilities.parseDate(this.model.get('timeline').get('startDate'));
            var startDateTimelineBeginWeekOrMonth = timeline.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
                ? Confluence.Roadmap.DateUtilities.getStartOfMonth(startDateTimelineActual)
                : Confluence.Roadmap.DateUtilities.getStartOfIsoWeek(startDateTimelineActual);

            bars.add(new Roadmap.Bar({
                rowIndex: firstLane.getNumberOfRows(),
                startDate: startDateTimelineBeginWeekOrMonth
            }));
        },

        /**
         * Append new Lane
         */
        onAddLaneClick: function(e) {
            e.preventDefault();

            var lanes = this.model.get('lanes'); // Draft code, need more validate here
            lanes.add(new Roadmap.Lane());
        },

        onAddMarkerClick: function(e) {
            const markers = this.model.get('markers');
            const timeline = this.model.get('timeline');
            const startDateTimeline = Confluence.Roadmap.DateUtilities.parseDate(timeline.get('startDate'));
            const startDateToRender = Confluence.Roadmap.DateUtilities.parseDate(startDateTimeline);
            if (timeline.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                startDateTimeline.setDate(15);
            } else {
                Confluence.Roadmap.DateUtilities.setIsoWeekday(startDateTimeline, 4);
            }

            markers.add(new Roadmap.Marker({
                markerDate: startDateToRender
            }));
        },

        onTimelineOptionChange: function (e) {
            var timeline = this.model.get('timeline');
            timeline.set({displayOption: this.$timelineSelect.val()});
        },

        _initVariables: function() {
            this.$timelineForm =  this.$el.find('#timeline-options-form');
            this.$startDateInput = this.$timelineForm.find('#timeline-startdate-input');
            this.$endDateInput = this.$timelineForm.find('#timeline-enddate-input');
            this.$timelineSelect = this.$timelineForm.find('#timeline-display-options-select');
            this.$timelineError = this.$timelineForm.find('#roadmap-timeline-error');
            this.newTimeline = new Roadmap.Timeline();
        },

        _clearTimelineErrors: function () {
            this.$timelineError.empty();
        },

        _showTimelineErrors: function () {
            var me = this;
            _.each(me.newTimeline.validationError, function (error) {
                me.$timelineError.append(error.message);
            });
        },

        _applyNewTimeline: function() {
            var timeline = this.model.get('timeline');
            timeline.set({
                startDate: Confluence.Roadmap.DateUtilities.parseDate(this.$startDateInput.val()),
                endDate: Confluence.Roadmap.DateUtilities.parseDate(this.$endDateInput.val())
            });
            this._showTimeLineFormat();
        },

        _showTimeLineFormat: function () {
            var timeline = this.model.get('timeline');
            this.$startDateInput.val(Confluence.Roadmap.DateUtilities.getDateStringFromDate(timeline.get('startDate'), Roadmap.SHORT_DATE_FORMAT));
            this.$endDateInput.val(Confluence.Roadmap.DateUtilities.getDateStringFromDate(timeline.get('endDate'), Roadmap.SHORT_DATE_FORMAT));
        },

        _validate: function () {
            this._clearTimelineErrors();
            if (this.newTimeline.isValid()) {
                this._applyNewTimeline();
            } else {
                this._showTimelineErrors();
            }
        },

        _setupTimelineOptionsForm: function() {
            const timeline = this.model.get('timeline');
            const startDate = Confluence.Roadmap.DateUtilities.parseDate(timeline.get('startDate'));
            const endDate = Confluence.Roadmap.DateUtilities.parseDate(timeline.get('endDate'));
            const startDateString = Confluence.Roadmap.DateUtilities.getDateStringFromDate(startDate, Roadmap.SHORT_DATE_FORMAT);
            const endDateString = Confluence.Roadmap.DateUtilities.getDateStringFromDate(endDate, Roadmap.SHORT_DATE_FORMAT);

            this.$startDateInput.val(startDateString);
            this.$endDateInput.val(endDateString);
            this.newTimeline.set({ startDateString: startDateString, endDateString: endDateString });
            this.$timelineSelect.val(timeline.get('displayOption'));

            //  Add AUI date picker for date fields
            var opsDatePicker = {
                overrideBrowserDefault: true,
                firstDay: 1 // Monday is first day of week.
            };
            this.$startDateInput.datePicker(opsDatePicker);
            this.$endDateInput.datePicker(opsDatePicker);

            Confluence.Roadmap.FieldUtilities.fixDatePickerFields(this.$timelineForm);

            var me = this;
            this.$startDateInput.on('change', function () {
                me.newTimeline.set({ startDateString: me.$startDateInput.val()});
                me._validate();
            });

            this.$endDateInput.on('change', function () {
                me.newTimeline.set({ endDateString: me.$endDateInput.val()});
                me._validate();

            });
        }
    });
})(AJS.$, window._);
