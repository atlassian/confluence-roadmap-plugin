(function () {
    /* global QUnit:false */
    'use strict';

    const { module, test } = QUnit;

    const { getPosXOnWeekTimeline, getWeekStartDateByPosition } =
        Confluence.Roadmap.Helper;

    module('Roadmap helper - getPosXOnWeekTimeline()', (hooks) => {
        let oldWeekWidth;
        hooks.beforeEach(() => {
            oldWeekWidth = window.Roadmap
                ? window.Roadmap.WEEK_WIDTH
                : undefined;

            window.Roadmap = window.Roadmap || {};
            window.Roadmap.WEEK_WIDTH = 70;
        });

        hooks.afterEach(() => {
            window.Roadmap.WEEK_WIDTH = oldWeekWidth;
        });

        test('getPosXOnWeekTimeline - start day is after timeline start day', (assert) => {
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const startDate = new Date(2015, 3, 20);
            const pos = getPosXOnWeekTimeline(timeline, startDate);
            assert.strictEqual(pos, window.Roadmap.WEEK_WIDTH * 2);
        });

        test('getPosXOnWeekTimeline - timeline start day is not Monday', (assert) => {
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const startDate = new Date(2015, 3, 20);
            const pos = getPosXOnWeekTimeline(timeline, startDate);
            assert.strictEqual(pos, window.Roadmap.WEEK_WIDTH * 2);
        });

        test('getPosXOnWeekTimeline - start day is before timeline start day', (assert) => {
            // Monday.
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const startDate = new Date(2015, 4 - 1, 3);
            const pos = getPosXOnWeekTimeline(timeline, startDate);
            assert.strictEqual(pos, -((3 * window.Roadmap.WEEK_WIDTH) / 7));
        });

        test('getWeekStartDateByPosition - 2 weeks exactly', (assert) => {
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const pos = 140; // 2 weeks
            const date = getWeekStartDateByPosition(timeline, pos);
            assert.strictEqual(date.toString(), new Date(2015, 4 - 1, 20).toString());
        });

        test('getWeekStartDateByPosition - over two weeks', (assert) => {
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const pos = 150; // > 2 weeks
            const date = getWeekStartDateByPosition(timeline, pos);
            assert.strictEqual(date.toString(), new Date(2015, 4 - 1, 21).toString());
        });

        test('getWeekStartDateByPosition - also over two weeks', (assert) => {
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const pos = 160; // > 2 weeks
            const date = getWeekStartDateByPosition(timeline, pos);
            assert.strictEqual(date.toString(), new Date(2015, 4 - 1, 22).toString());
        });

        test('getWeekStartDateByPosition - negative', (assert) => {
            const timeline = { startDate: new Date(2015, 4 - 1, 6) };
            const pos = -50; // > 2 weeks
            const date = getWeekStartDateByPosition(timeline, pos);
            assert.strictEqual(date.toString(), new Date(2015, 4 - 1, 1).toString());
        });
    });
})();
