(function() {
    /* global QUnit:false */
    'use strict';

    const { module, test } = QUnit;

    const { parseDate, convertToWeekDuration, convertToMonthDuration, areInSameWeek, addMonths, diffInYears } =
        Confluence.Roadmap.DateUtilities;

    const dateString = '2015-01-14';

    module('Roadmap utilities', () => {
        test('parseDate - full date', (assert) => {
            const dateString = '2015-13-01';
            const date = parseDate(dateString);
            assert.strictEqual(date, null);
        });

        test('parseDate - single digit', (assert) => {
            const dateString = '2015-1-01';
            const date = parseDate(dateString);
            assert.strictEqual(date.toString(), new Date(dateString).toString());
        });

        test('parseDate - empty date', (assert) => {
            const dateString = '';
            const date = parseDate(dateString);
            assert.strictEqual(date, null);
        });

        test('parseDate - another full date?', (assert) => {
            const dateString = '2015-12-01';
            const date = parseDate(dateString);
            assert.strictEqual(date.toString(), new Date(2015, 11, 1).toString());
        });

        test('parseDate - two digit year', (assert) => {
            const dateString = '15-12-01';
            const date = parseDate(dateString);
            assert.strictEqual(date.toString(), new Date(2015, 11, 1).toString());
        });

        test('parseDate - date object', (assert) => {
            const date = new Date(2015, 11, 1);
            const dateParsed = parseDate(date);
            assert.strictEqual(dateParsed.toString(), date.toString());
        });

        test('parseDate - storage format date', (assert) => {
            const dateString = '2015-12-01 00:00:00';
            const date = parseDate(dateString);
            assert.strictEqual(date.toString(), new Date(2015, 11, 1).toString());
        });

        test('areInSameWeek - same week', (assert) => {
            const date1 = new Date(2015, 10, 5);
            const date2 = new Date(2015, 10, 11);
            assert.false(areInSameWeek(date1, date2));
        });

        test('areInSameWeek - different week', (assert) => {
            const date1 = new Date(2015, 10, 5);
            const date2 = new Date(2015, 10, 12);
            assert.false(areInSameWeek(date1, date2));
        });

        test('addMonths months', (assert) => {
            const startDate = new Date(2000, 2, 31);
            const endDate = new Date(2000, 3, 30)
            assert.strictEqual(addMonths(startDate, 1).toString(), endDate.toString());
        });

        test('addMonths leap year', (assert) => {
            const startDate = new Date(2023, 2, 31);
            const endDate = new Date(2024, 1, 29)
            assert.strictEqual(addMonths(startDate, 11).toString(), endDate.toString());
        });

        test('diffInYears 5 years', (assert) => {
            const startDate = new Date(2019, 1, 1);
            const endDate = new Date(2024, 1, 1)
            assert.strictEqual(diffInYears(startDate, endDate), 5);
        });

        test('diffInYears 4 years', (assert) => {
            const startDate = new Date(2019, 1, 2);
            const endDate = new Date(2024, 1, 1)
            assert.strictEqual(diffInYears(startDate, endDate), 4);
        });

        [0.85, 1.25, 1.85, 2.2].forEach((monthDuration) => {
            test(`convertTimeline - month duration ${monthDuration}`, (assert) => {
                const startDate = parseDate(dateString);
                const weekDuration = convertToWeekDuration(
                    startDate,
                    monthDuration
                );
                const convertMonthDuration = convertToMonthDuration(
                    startDate,
                    weekDuration
                );
                assert.equal(convertMonthDuration.toFixed(2), monthDuration);
            });
        });
    });
})();
