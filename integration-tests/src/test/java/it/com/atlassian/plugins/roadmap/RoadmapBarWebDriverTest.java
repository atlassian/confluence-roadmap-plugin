package it.com.atlassian.plugins.roadmap;

import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditContentPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import it.com.atlassian.plugins.roadmap.pageobjects.BarDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.BarPageLinkDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.RoadmapEditorDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.ViewPageRoadmap;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ADMIN_PERMISSIONS;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static it.com.atlassian.plugins.roadmap.utils.NumberOfStringMatchers.hasNumberOfSubstrings;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * This class create to test for stuffs relate to Bar
 * - Bar creation
 * - Bar delete
 * - Bar drag and drop
 * - Bar resize
 * - Bar view detail information
 */
@RunWith(ConfluenceStatelessTestRunner.class)
public class RoadmapBarWebDriverTest extends AbstractRoadmapWebDriverTest {
    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static UserFixture admin = userFixture()
            .globalPermission(GlobalPermission.CONFLUENCE_ADMIN)
            .build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .permission(admin, ADMIN_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture page = pageFixture()
            .space(space)
            .author(admin)
            .title("Some Roadmap Page")
            .build();
    @Fixture
    private static PageFixture addBarRoadmapPage = pageFixture()
            .space(space)
            .author(admin)
            .title("Add Bar in Roadmap")
            .contentFromClasspath("roadmap-data/roadmapAddBar.xml")
            .build();
    @Fixture
    private static SpaceFixture anotherSpace = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();

    @Test
    public void addBarToRoadmap() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), addBarRoadmapPage.get());
        final RoadmapEditorDialog roadmapEditorHtmlDialog = openRoadmapFromMacroPlaceholder(editingPage);

        roadmapEditorHtmlDialog.clickAddBar();

        roadmapEditorHtmlDialog.clickSave();
        editingPage.getEditor().clickSaveAndWaitForPageChange();

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        final Content addedBarPage = restClient.getAdminSession()
                .contentService()
                .find(new Expansion("body.storage"))
                .withId(addBarRoadmapPage.get().getId())
                .fetchOrNull();

        assertThat(addedBarPage, notNullValue());

        assertThat(
                addedBarPage.getBody().get(ContentRepresentation.STORAGE).getValue(),
                hasNumberOfSubstrings("New%20Bar", 1)
        );
    }

    @Test
    public void testDeleteBar() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        String barTitle = "Bar 1";
        final BarDialog barDialog = roadmapEditorDialog.clickOnBar(barTitle);
        barDialog.clickDelete();

        List<String> barTitles = roadmapEditorDialog.getAllBarTitles();
        // Check deleted bar is not existing in edit mode
        assertThat(
                "Deleted bar should not existing in the bar list", barTitles, not(hasItem(barTitle))
        );

        roadmapEditorDialog.clickSave();
        final TimelinePlanner roadmap = getRoadmapFromEditingPage(editingPage);
        // check deleted bar is not existing in data when saved
        assertFalse("Deleted bar should not existing in the bar list", isBarExisting(roadmap, barTitle));

        editingPage.getEditor().clickCancelAndWaitForPageReload();
    }

    @Test
    public void testRenameBar() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        String barTitle = "Bar 1";
        final BarDialog barDialog = roadmapEditorDialog.clickOnBar(barTitle);

        barDialog.clickRename().inputBarTitle("  ");
        // Check save button is disabled when input is empty also with spaces
        assertFalse("Save button should disabled", barDialog.isSaveButtonEnabled());

        String newBarTitle = "New Title - Bar 1";
        barDialog.inputBarTitle(newBarTitle);
        // Check save button is enabled when input is not empty
        assertTrue("Save button should enabled", barDialog.isSaveButtonEnabled());
        barDialog.clickSave();

        final List<String> barTitles = roadmapEditorDialog.getAllBarTitles();
        // Check new bar title is existed in edit mode
        assertThat("New bar title should existed in the bar list", barTitles, hasItem(newBarTitle));

        roadmapEditorDialog.clickSave();
        final TimelinePlanner roadmap = getRoadmapFromEditingPage(editingPage);
        // Check new bar title is existed in in data when saved
        assertTrue("New bar title should existed in the bar list", isBarExisting(roadmap, newBarTitle));

        editingPage.getEditor().clickCancelAndWaitForPageReload();
    }

    @Test
    public void addDescription() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        final BarDialog barDialog = roadmapEditorDialog.clickOnBar("Bar 1");

        // Null description case
        String emptyDescription = "Click to add description.";
        barDialog.clickDescription();
        barDialog.inputDescription("");
        barDialog.clickSave();
        assertThat("Should show empty description string", barDialog.getDescriptionText(), is(emptyDescription));

        // Input value with XSS
        String xssString = "<script>alert('test');</script>";
        barDialog.clickDescription();
        barDialog.inputDescription(xssString);
        barDialog.clickSave();
        assertThat("Description with content is xss string should be shown", barDialog.getDescriptionText(), is(xssString));

        roadmapEditorDialog.clickSave();
        editingPage.getEditor().clickCancelAndWaitForPageReload();
    }

    @Test
    public void createPageLinkEditModeInNonDefaultSpace() {
        String barTitle = "Bar 2";
        String createdPageTitle = "New page link from Roadmap - Edit Mode";
        String createdPageContent = "Here is content of " + createdPageTitle;

        final EditContentPage editingPage = product.loginAndEdit(user.get(), anotherSpace.get().getHomepageRef().get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);
        BarPageLinkDialog barEditDialog = roadmapEditorDialog.clickOnBar(barTitle);
        barEditDialog = barEditDialog.createNewPageAndRebind(createdPageTitle, createdPageContent);
        assertThat("Pagelink should be shown in roadmap bar - editmode", barEditDialog.getPageLinkTitle(), is(createdPageTitle));

        roadmapEditorDialog.clickSave();
        final ViewPage viewPage = editingPage.save();
        final ViewPageRoadmap viewPageRoadmap = product.getPageBinder().bind(ViewPageRoadmap.class, viewPage);
        assertThat("View should show a roadmap svg", viewPageRoadmap.hasRoadmap(), is(true));
        final BarPageLinkDialog barViewDialog = viewPageRoadmap.clickOnBar(barTitle);
        assertThat("Pagelink should be shown in roadmap bar - viewmode", barViewDialog.getPageLinkTitle(), is(createdPageTitle));
    }

    private boolean isBarExisting(TimelinePlanner roadmap, String barTitle) {
        return roadmap.getLanes()
                .stream()
                .flatMap(lane -> lane.getBars().stream())
                .anyMatch(bar -> barTitle.equals(bar.getTitle()));
    }

}