package it.com.atlassian.plugins.roadmap.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public final class RoadmapStringUtils {
    public static Map<String, String> getParameters(String parametersString, String paramSeparationString,
                                                    String valueSeparationString) {
        Map<String, String> params = new HashMap<String, String>();
        StringTokenizer tokenizer = new StringTokenizer(parametersString, paramSeparationString);
        while (tokenizer.hasMoreTokens()) {
            String paramDefinition = tokenizer.nextToken();
            StringTokenizer definitionTokenizer = new StringTokenizer(paramDefinition, valueSeparationString);

            if (definitionTokenizer.hasMoreTokens()) {
                final String paramName = definitionTokenizer.nextToken();
                final String paramValue;

                if (!definitionTokenizer.hasMoreTokens())
                    paramValue = "true";
                else
                    paramValue = StringUtils.trim(definitionTokenizer.nextToken());
                params.put(paramName, paramValue);
            }
        }

        return params;
    }
}
