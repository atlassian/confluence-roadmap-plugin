package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.BlueprintDialog;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CreatePage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertTrue;

public class BarPageLinkDialog extends Dialog {
    @Inject
    private JavascriptExecutor javascriptExecutor;

    public BarPageLinkDialog() {
        super("inline-dialog-roadmap-dialog");
    }

    public String getPageLinkTitle() {
        PageElement linkPageTitle = getDialog().find(By.cssSelector(".link-page-title"), TimeoutType.PAGE_LOAD);
        return linkPageTitle.getText();
    }

    public BarPageLinkDialog createNewPageAndRebind(String createdPageTitle, String createdPageContent) {
        String baseWindow = driver.getWindowHandle();
        BlueprintDialog blueprintDialog = clickCreatePageLink();
        CreatePage createPage = blueprintDialog.submitForCreatePageEditor();
        createPage.setTitle(createdPageTitle);
        createPage.getEditor().getContent().setContent(createdPageContent);
        createPage.save();
        driver.close();

        driver.switchTo().window(baseWindow);
        javascriptExecutor.executeScript("window.dispatchEvent(new Event('focus'))"); //make sure driver always fires 'focus' event
        return pageBinder.bind(BarPageLinkDialog.class);
    }

    private BlueprintDialog clickCreatePageLink() {
        String parentWindow = driver.getWindowHandle();
        PageElement newPage = find(".link-new-page").click();
        waitUntilTrue(newPage.timed().isVisible());
        switchToNewPage(parentWindow);
        Poller.waitUntilTrue(pageElementFinder.find(By.cssSelector(".create-dialog-create-button")).timed().isEnabled());
        assertTrue("Create blank page must be appeared", isBlueprintItemPresent("com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page"));
        return pageBinder.bind(BlueprintDialog.class);
    }

    private void switchToNewPage(String parentWindow) {
        Set<String> set = driver.getWindowHandles();
        set.remove(parentWindow);
        String childWindow = (String) set.toArray()[0];
        Assert.assertNotEquals("Child page should be activated", parentWindow, childWindow);
        driver.switchTo().window(childWindow);
    }

    private boolean isBlueprintItemPresent(String blueprintModuleCompleteKey) {
        PageElement templateList = pageElementFinder.find(By.className("templates"), TimeoutType.SLOW_PAGE_LOAD);
        waitUntilTrue(templateList.timed().isVisible());
        return templateList.findAll(By.cssSelector("[data-item-module-complete-key='" + blueprintModuleCompleteKey + "']")).size() > 0;
    }
}
