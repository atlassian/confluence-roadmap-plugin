package it.com.atlassian.plugins.roadmap;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditContentPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditorPage;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.plugins.roadmap.TimelinePlannerJsonBuilder;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import it.com.atlassian.plugins.roadmap.pageobjects.LaneDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.RoadmapEditorDialog;
import it.com.atlassian.plugins.roadmap.utils.RoadmapStringUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Before;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.containsString;

public abstract class AbstractRoadmapWebDriverTest {
    @Inject
    static ConfluenceTestedProduct product;
    @Inject
    static ConfluenceRestClient restClient;
    @Inject
    static ConfluenceRpcClient rpcClient;

    static final String[] ROADMAP_MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    private static final String ROADMAP_MACRO = "roadmap";
    private static final String DARK_FEATURE_LIGHT_EDIOTOR = "lighter-editor-v1";

    @Before
    public void init() {
        restClient.getAdminSession().darkFeature().disableSiteFeature(DARK_FEATURE_LIGHT_EDIOTOR);
    }

    protected RoadmapEditorDialog openRoadmapEditorDialog(EditorPage editingPage) {
        waitUntilTrue(editingPage.getEditor().getContent().isContentVisible());
        editingPage.getEditor().openMacroBrowser();

        final RoadmapEditorDialog roadmapEditorDialog = product.getPageBinder().bind(RoadmapEditorDialog.class);
        roadmapEditorDialog.open();
        waitUntilTrue(roadmapEditorDialog.isRoadmapEditorDialogVisible());

        return roadmapEditorDialog;
    }

    protected RoadmapEditorDialog openRoadmapFromMacroPlaceholder(EditContentPage editingPage) {
        waitUntil(editingPage.getEditor().getContent().getTimedHtml(), containsString("data-macro-name=\"roadmap\""));
        editingPage.getEditor().getContent().doubleClickEditInlineMacro(ROADMAP_MACRO);
        return product.getPageBinder().bind(RoadmapEditorDialog.class);
    }

    private TimedQuery<Boolean> roadmapMacroPresent(EditContentPage editingPage) {
        return editingPage.getEditor().getContent().hasMacroPlaceholder("roadmap");
    }

    protected TimelinePlanner getRoadmapFromEditingPage(EditContentPage editingPage) {
        waitUntilTrue(roadmapMacroPresent(editingPage));
        waitUntilTrue(Conditions.forSupplier(() ->
                editingPage.getEditor().getContent().macroPlaceholderFor("roadmap").stream()
                        .anyMatch(macroPlaceholder -> {
                            String parameterString = macroPlaceholder.getAttribute("data-macro-parameters");
                            final Map<String, String> roadmapParams = getParameters(parameterString);
                            return roadmapParams.containsKey("source");
                        })
        ));
        final Map<String, String> roadmapParams = editingPage.getEditor().getContent().macroPlaceholderFor("roadmap").stream()
                .map(macroPlaceholder -> {
                    String parameterString = macroPlaceholder.getAttribute("data-macro-parameters");
                    return getParameters(parameterString);
                })
                .filter(params -> params.containsKey("source"))
                .findFirst().orElseThrow(() -> new AssertionError("Missing source"));

        return TimelinePlannerJsonBuilder.fromJson(roadmapParams.get("source"));
    }

    private static Map<String, String> getParameters(String macroParamString) {
        return RoadmapStringUtils.getParameters(macroParamString, "|", "=");
    }

    public Content createPageFromResourceFile(UserWithDetails user, Space space, String title, String resourceFile) throws IOException {
        final String pageContent = FileUtils.readFileToString(new File(getClass().getResource(resourceFile).getFile()));
        return restClient.createSession(user)
                .contentService()
                .create(
                        Content.builder()
                                .space(space)
                                .title(title)
                                .type(ContentType.PAGE)
                                .body(pageContent, ContentRepresentation.STORAGE)
                                .build()
                );
    }

    protected LaneDialog openDialogForFirstLane() {
        final LaneDialog laneDialog = product.getPageBinder().bind(LaneDialog.class);
        laneDialog.openForFirstLane();
        return laneDialog;
    }

    protected LaneDialog openDialogForLane(String laneTitle) {
        final LaneDialog laneDialog = product.getPageBinder().bind(LaneDialog.class);
        laneDialog.open(laneTitle);
        return laneDialog;
    }
}
