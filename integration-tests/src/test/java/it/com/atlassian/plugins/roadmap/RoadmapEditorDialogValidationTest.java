package it.com.atlassian.plugins.roadmap;

import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditContentPage;
import it.com.atlassian.plugins.roadmap.pageobjects.RoadmapEditorDialog;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ADMIN_PERMISSIONS;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static it.com.atlassian.plugins.roadmap.RoadmapWebDriverTest.getDateStringFromCalendar;
import static it.com.atlassian.plugins.roadmap.RoadmapWebDriverTest.getShortDateStringFromCalendar;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(ConfluenceStatelessTestRunner.class)
public class RoadmapEditorDialogValidationTest extends AbstractRoadmapWebDriverTest {
    @Fixture
    private static final UserFixture user = userFixture().build();
    @Fixture
    private static final UserFixture admin = userFixture()
            .globalPermission(GlobalPermission.CONFLUENCE_ADMIN)
            .build();
    @Fixture
    private static final SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .permission(admin, ADMIN_PERMISSIONS)
            .build();
    @Fixture
    private static final PageFixture page = pageFixture()
            .space(space)
            .author(admin)
            .title("Some Roadmap Page")
            .build();

    @Test
    public void testStartDateMoreThanFiveYearsAgo() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        openRoadmapEditorDialog(editingPage)
                .typeStartDate("14-01-01")
                .assertHasErrorMessage("You cannot plan more than 5 years ahead");
    }

    @Test
    public void testDateInWrongFormat() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 2);

        openRoadmapEditorDialog(editingPage)
                .typeStartDate(getShortDateStringFromCalendar(calendar))
                .typeEndDate("foo")
                .assertHasErrorMessage("Enter dates in yy-mm-dd");
    }

    @Test
    public void testStartDateBeforeEndDate() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 2);

        roadmapEditorDialog.typeStartDate(getShortDateStringFromCalendar(calendar));
        assertThat(roadmapEditorDialog.getStartDate(), is(getDateStringFromCalendar(calendar)));

        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DAY_OF_MONTH, 2);

        roadmapEditorDialog.typeEndDate(getShortDateStringFromCalendar(calendar));
        roadmapEditorDialog.assertHasErrorMessage("Start date needs to be before the end date");
    }
}
