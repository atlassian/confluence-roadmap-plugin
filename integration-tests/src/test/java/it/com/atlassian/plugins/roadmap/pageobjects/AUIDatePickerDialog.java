package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class AUIDatePickerDialog {
    private final String dpPopupUuid;

    @Inject
    private ConfluenceTestedProduct product;

    @Inject
    private PageElementFinder pageElementFinder;

    public AUIDatePickerDialog(String dpPopupUuid) {
        this.dpPopupUuid = dpPopupUuid;
    }

    public void chooseSelectedDay() {
        // match both AUI7 & AUI8 component. See also CONFSRVDEV-10987
        final By currentDateSelector = By.cssSelector("div[data-aui-dp-popup-uuid='" + this.dpPopupUuid + "'] .ui-datepicker-current-day,.aui-datepicker-dialog[id='" + this.dpPopupUuid + "'] .ui-datepicker-current-day");
        Poller.waitUntilTrue(pageElementFinder.find(currentDateSelector).timed().isVisible());
        product.getTester().getDriver().findElement(currentDateSelector).click();
    }
}
