package it.com.atlassian.plugins.roadmap;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.component.editor.MacroPlaceholder;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditContentPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import it.com.atlassian.plugins.roadmap.pageobjects.LaneDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.LaneRenameDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.MarkerDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.MarkerRenameDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.RoadmapEditorDialog;
import it.com.atlassian.plugins.roadmap.pageobjects.RoadmapMacroPropertyPanel;
import it.com.atlassian.plugins.roadmap.pageobjects.ViewPageRoadmap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ADMIN_PERMISSIONS;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(ConfluenceStatelessTestRunner.class)
public class RoadmapWebDriverTest extends AbstractRoadmapWebDriverTest {
    @Inject
    private static PageBinder pageBinder;
    @Inject
    private static GlobalElementFinder finder;

    @Fixture
    private static final UserFixture user = userFixture().build();
    @Fixture
    private static final UserFixture admin = userFixture()
            .globalPermission(GlobalPermission.CONFLUENCE_ADMIN)
            .build();
    @Fixture
    private static final SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .permission(admin, ADMIN_PERMISSIONS)
            .build();
    @Fixture
    private static final PageFixture page = pageFixture()
            .space(space)
            .author(admin)
            .title("Some Roadmap Page")
            .build();

    @Test
    public void showDeprecationDialogWhenEditOldRoadmap() throws IOException {
        // Create a page with old roadmap
        final Content oldRoadmapPage = createPageFromResourceFile(
                admin.get(), space.get(), "Old Roadmap Macro", "/roadmap-data/roadmapOldVersion.xml"
        );

        final EditContentPage editPage = product.loginAndEdit(admin.get(), oldRoadmapPage);
        final MacroPlaceholder macroPlaceholder = editPage.getEditor()
                .getContent()
                .macroPlaceholderFor("roadmap")
                .iterator()
                .next();

        // Click on the placeholder to show the Property Panel
        macroPlaceholder.click();

        // Bind the Property Panel and click on Edit button.
        final RoadmapMacroPropertyPanel deprecationDialog = product.getPageBinder().bind(RoadmapMacroPropertyPanel.class);
        deprecationDialog.edit();

        // Assert that the dialog is presented
        final By dialogSelector = By.cssSelector("#roadmap-deprecation-dialog .aui-dialog2-content");
        waitUntilTrue(finder.find(dialogSelector).timed().isVisible());
        assertTrue(product.getTester().getDriver().findElement(dialogSelector).isDisplayed());
    }

    @Test
    public void testTimelineOptionsDialog() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        // check roadmap timeline when create new roadmap
        Calendar calendar = Calendar.getInstance();
        String firstTimeline = roadmapEditorDialog.getFirstTimeline();
        String startMonthYear = getMonthYearFromCalendar(calendar, true);
        assertThat("First timeline should be the same with start month year", firstTimeline, is(startMonthYear));
        String startDate = getDateStringFromCalendar(calendar);

        calendar.add(Calendar.MONTH, 11);
        String lastTimeline = roadmapEditorDialog.getLastTimeline();
        String endMonthYear = getMonthYearFromCalendar(calendar, false);
        assertThat("Last timeline should be end month year", lastTimeline, is(endMonthYear));

        String endDate = getDateStringFromCalendar(calendar);

        // check Start date, End date inputs in Timeline Options dialog display value from Roadmap Timeline
        assertThat("Start date input value should be same with start date", roadmapEditorDialog.getStartDate(), is(startDate));
        assertThat("End date input value should be same with end date", roadmapEditorDialog.getEndDate(), is(endDate));

        // check roadmap timeline when change Start date, End date
        calendar.add(Calendar.MONTH, -14);
        roadmapEditorDialog.setStartDate(getDateStringFromCalendar(calendar));
        startMonthYear = getMonthYearFromCalendar(calendar, true);

        calendar.add(Calendar.MONTH, 16);
        roadmapEditorDialog.setEndDate(getDateStringFromCalendar(calendar));
        endMonthYear = getMonthYearFromCalendar(calendar, false);

        firstTimeline = roadmapEditorDialog.getFirstTimeline();
        assertThat("First timeline should be the same with start month year after changed", firstTimeline, is(startMonthYear));

        lastTimeline = roadmapEditorDialog.getLastTimeline();
        assertThat("Last timeline should be end month year after changed", lastTimeline, is(endMonthYear));
    }

    @Test
    @ResetFixtures(value = {"page"}, when = ResetFixtures.When.BOTH)
    public void testDeleteLane() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        LaneDialog laneDialog = openDialogForFirstLane();
        laneDialog.clickDelete();

        final List<String> lanesTitle = roadmapEditorDialog.getAllLanesTitle();
        // check number of lanes after delete in edit mode
        assertThat("Should only has 1 lane after delete", lanesTitle, hasSize(1));

        laneDialog = openDialogForFirstLane();
        // check do not display delete button when has 1 lane
        waitUntilFalse("Should not display delete button", laneDialog.isDeleteButtonPresent());

        roadmapEditorDialog.clickSave();
        final TimelinePlanner roadmap = getRoadmapFromEditingPage(editingPage);
        // check only has 1 lane in data
        assertThat("Should only has 1 lane after delete", roadmap.getLanes(), hasSize(1));

        editingPage.getEditor().clickCancelAndWaitForPageReload();
    }

    @Test
    public void testRenameLane() {
        final String renamedTitle = "Lane 1 modified";
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        final LaneDialog laneDialog = openDialogForFirstLane();
        final LaneRenameDialog laneRenameDialog = laneDialog.clickRename();
        laneRenameDialog.inputTextTitle(renamedTitle);
        laneRenameDialog.clickSave();

        assertThat(roadmapEditorDialog.getAllLanesTitle(), hasItem(renamedTitle));
    }

    @Test
    @ResetFixtures(value = {"page"}, when = ResetFixtures.When.BOTH)
    public void testChangeLaneColor() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        String laneTitle = "Lane 2";
        String oldColor = "rgb(59, 127, 196)"; // #3b7fc4
        String laneColor = roadmapEditorDialog.getLaneColor(laneTitle);
        // check Lane's color before change
        assertThat("Lane 2's color should be rgb(59, 127, 196)", laneColor, is(oldColor));

        String newColor = "rgb(208, 68, 55)"; // #d04437
        LaneDialog laneDialog = openDialogForLane(laneTitle);
        laneDialog.changeColor(newColor);

        laneColor = roadmapEditorDialog.getLaneColor(laneTitle);
        // check Lane's color after change color in edit mode
        assertThat("Lane 2's color should be rgb(208, 68, 55)", laneColor, is(newColor));

        roadmapEditorDialog.clickSave();

        final TimelinePlanner roadmap = getRoadmapFromEditingPage(editingPage);
        // check Lane's color after change color in data
        laneColor = roadmap.getLanes().get(1).getColor().getLane();
        assertThat("Lane 2's color should be #d04437", laneColor, is("#d04437"));

        editingPage.getEditor().clickCancelAndWaitForPageReload();
    }

    @Test
    @ResetFixtures(value = "page", when = ResetFixtures.When.BEFORE)
    public void testRenameMarker() {
        String marker = "Marker 1";
        String markerEditTitle = "Marker 2";

        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);
        assertTrue("Marker should exist", roadmapEditorDialog.containMarker(marker));
        roadmapEditorDialog.scrollMarkerIntoView(marker);

        final MarkerDialog markerDialog = roadmapEditorDialog.clickOnMarker(marker);
        final MarkerRenameDialog markerRenameDialog = markerDialog.clickRename();
        markerRenameDialog.inputTextTitle(markerEditTitle);
        markerRenameDialog.clickSave();
        assertTrue("Marker title should be updated in editmode", roadmapEditorDialog.containMarker(markerEditTitle));

        roadmapEditorDialog.clickSave();
        editingPage.getEditor().clickSaveAndWaitForPageChange();
        final ViewPage viewPage = pageBinder.bind(ViewPage.class);
        final ViewPageRoadmap viewPageRoadmap = product.getPageBinder().bind(ViewPageRoadmap.class, viewPage);
        assertThat("View should show a roadmap svg", viewPageRoadmap.hasRoadmap(), is(true));
        assertThat("Marker title should be updated in viewmode", viewPage.getTextContent(), containsString(markerEditTitle));
    }

    @Test
    public void testDeleteMarker() {
        String marker = "Marker 1";

        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);
        assertTrue("Marker should be existed", roadmapEditorDialog.containMarker(marker));
        roadmapEditorDialog.scrollMarkerIntoView(marker);

        final MarkerDialog markerDialog = roadmapEditorDialog.clickOnMarker(marker);
        markerDialog.clickDelete();
        assertFalse("Marker should be removed in editmode", roadmapEditorDialog.containMarker(marker));

        roadmapEditorDialog.clickSave();
        editingPage.getEditor().clickSaveAndWaitForPageChange();
        final ViewPage viewPage = pageBinder.bind(ViewPage.class);
        final ViewPageRoadmap viewPageRoadmap = product.getPageBinder().bind(ViewPageRoadmap.class, viewPage);
        assertThat("View should show a roadmap svg", viewPageRoadmap.hasRoadmap(), is(true));
        assertThat("Marker should be removed in viewmode", viewPage.getTextContent(), not(containsString(marker)));
    }

    @Test
    public void testInputStartDateAndEndDateTimeline() {
        final EditContentPage editingPage = product.loginAndEdit(user.get(), page.get());
        final RoadmapEditorDialog roadmapEditorDialog = openRoadmapEditorDialog(editingPage);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 2);

        roadmapEditorDialog.typeStartDate(getShortDateStringFromCalendar(calendar));
        assertThat(roadmapEditorDialog.getStartDate(), is(getDateStringFromCalendar(calendar)));

        calendar.add(Calendar.YEAR, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        roadmapEditorDialog.typeEndDate(getShortDateStringFromCalendar(calendar));
        assertThat(roadmapEditorDialog.getEndDate(), is(getDateStringFromCalendar(calendar)));
    }

    private String getMonthYearFromCalendar(Calendar calendar, boolean isStartDate) {
        String monthYear = ROADMAP_MONTHS[calendar.get(Calendar.MONTH)];
        if (isStartDate || calendar.get(Calendar.MONTH) == 0) {
            monthYear = "" + calendar.get(Calendar.YEAR) + "\n" + monthYear;
        }
        return monthYear;
    }

    static String getDateStringFromCalendar(Calendar calendar) {
        return "" + calendar.get(Calendar.YEAR) + "-" +
                String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" +
                String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Gets a date with a 2 digit year.
     */
    static String getShortDateStringFromCalendar(Calendar calendar) {
        return "" + (calendar.get(Calendar.YEAR) % 100) + "-" +
                String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" +
                String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
    }
}
