package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public class ViewPageRoadmap extends ViewPage {
    @ElementBy(cssSelector = "div.roadmap-macro-view svg")
    private PageElement macroRoadmap;

    public ViewPageRoadmap(ViewPage viewPage) {
        super(viewPage.getPageEntity());
    }

    public boolean hasRoadmap() {
        return macroRoadmap.isVisible();
    }

    public BarPageLinkDialog clickOnBar(String barTitle) {
        PageElement barElement = getMainContent().find(By.cssSelector(String.format(".bar-title[title='%s']", barTitle)));
        barElement.click();
        return pageBinder.bind(BarPageLinkDialog.class);
    }

}
